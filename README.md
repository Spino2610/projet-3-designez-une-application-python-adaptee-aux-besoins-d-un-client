# OpenClassroom Projet 3 

# Designez une application Python adaptée aux besoins d un client

## Récapitulatif

Ce projet étudiant a pour but de créer une plateforme en ligne qui favorise l'apprentissage et le soutien scolaire en connectant les élèves en difficulté avec des tuteurs bénévoles, tout en offrant des fonctionnalités de coordination et de communication pour une expérience éducative plus enrichissante.

Le projet HomeSkolar est une collaboration entre CodeIguanas, une entreprise de services du numérique, et l'association HomeSkolar. L'objectif principal est de concevoir et de développer un site web pour HomeSkolar. Cette association met en relation des enfants en difficulté scolaire avec des tuteurs bénévoles. Le site web vise à faciliter la communication et la coordination entre les élèves et les tuteurs, ainsi qu'à offrir des fonctionnalités pour gérer les rendez-vous, les tâches, et les communications.

## Objectifs

Développer un site web pour permettre aux élèves et aux tuteurs de gérer leur compte, de communiquer, de planifier des rencontres, et de gérer des tâches.

Faciliter l'accès à un soutien scolaire en ligne pour les élèves en difficulté, en leur permettant d'obtenir de l'aide de tuteurs bénévoles.

Mettre en place des outils de coordination efficaces pour que les élèves et les tuteurs puissent organiser leurs rendez-vous et suivre leurs progrès.

## Étapes Clés

**Définition des Besoins Client grâce à l'élaboration un cahier des charges détaillé comprenant :**

 * Une liste des spécifications fonctionnelles, présentant ce que le produit va réaliser.
 * Une liste des spécifications techniques, présentant les technologies utilisées et comment la technique répond aux besoins du client.
 * Un diagramme de classes utilisant la nomenclature UML.
---
**Création d'un Backlog Produit intégrant:**

 * Des user stories avec critère(s) d’acceptation.
 * Les user stories doivent être priorisées
 * Une estimation en temps de réalisation.
---
**Un support de présentation incluant :**

 * Le contexte du projet.
 * Les fonctionnalités du site web.
 * Les choix techniques pour le développement.

## Utilisation du Projet

Les élèves peuvent s'inscrire, communiquer, planifier des rencontres et suivre leurs devoirs.

Les tuteurs peuvent également s'inscrire, communiquer, planifier des rencontres et attribuer des tâches aux élèves.
